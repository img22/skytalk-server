import { Request, Response } from "express";

import BusinessCtrl from "./business";
import CityCtrl from "./city";
import CityOptionCtrl from "./cityOption";

interface IHttp {
  [key: string]: any;
}

interface IRes extends Response {
  [key: string]: any;
}

interface IReq extends Request {
  [key: string]: any;
}

const http: IHttp = {};

http.get = async (req: IReq, res: IRes) => {
  const businesses = await BusinessCtrl.model
    .find({ city: req.query.city, state: req.query.state })
    .lean();

  const city = await CityCtrl.model
    .findOne({ name: req.query.city, country: "US" })
    .lean();

  res.response = { businesses, city };
};

http.cities = async (req: IReq, res: IRes) => {
  const cities = await CityOptionCtrl.model.find().lean();
  res.response = { cities };
};

// http.post = async (req: IReq, res: IRes) => {
//   await validator.post(req.yup, req.body);

//   const body = req.body;
//   const user = await BusinessCtrl.model.create(body);

//   res.data = user;
// };

export default http;
