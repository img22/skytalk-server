import { Document, Model } from "mongoose";

import BusinessModel from "@schemas/Business";

class Business {
  public model: Model<Document>;

  constructor() {
    this.model = BusinessModel;
  }
}

export default new Business();
