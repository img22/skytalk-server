import { Document, Model } from "mongoose";

import CityModel from "@schemas/City";

class City {
  public model: Model<Document>;

  constructor() {
    this.model = CityModel;
  }
}

export default new City();
