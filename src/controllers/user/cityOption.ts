import { Document, Model } from "mongoose";

import CityOptionModel from "@schemas/CityOption";

class CityOption {
  public model: Model<Document>;

  constructor() {
    this.model = CityOptionModel;
  }
}

export default new CityOption();
