import { model, Schema } from "mongoose";

const schema = new Schema(
  {
    city: { type: String, required: true },
    state: { type: String, required: true },
  },
  { minimize: true, timestamps: true }
);

export default model("Cityoption", schema);
