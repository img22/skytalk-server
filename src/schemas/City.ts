import { model, Schema } from "mongoose";

const schema = new Schema(
  {
    country: { type: String, required: true },
    name: { type: String, required: true },
    lat: { type: String, required: true },
    lng: { type: String, required: true },
  },
  { minimize: true, timestamps: true }
);

export default model("City", schema);
