## Skytalk server application

### Prerequisites

- MongoDB installed and running
- Yarn or NPM and Node

### Getting started

Clone this repository

Entring inside the project folder and installing the dependencies

```sh
cd skytalk-server && yarn install
```

Development

```sh
yarn start:dev
```

Production

```sh
start:prod
```

### Setting up Yelp Data

- Download the Yelp dataset [here](https://www.yelp.com/dataset)
- Login to your local `mongodb` and create a database called `skytalk`
- Import the Yelp dataset into the database by running `mongoimport --db skytalk --collection businesses --file yelp_academic_dataset_business.json` (`yelp_academic_dataset_business.json` is the downloaded Yelp dataset)
- Import the list of cities by running `mongoimport --db skytalk --collection cityoptions --file ./city-options.json --type json --jsonArray` (`city-options.json` is provided in the root directory of this repository)

### Note

- Make sure the [skytalk intel p2p](https://gitlab.com/img22/skytalk-p2p-server) is running in a separate process for the chat application to work
- The client application can be found [here](https://gitlab.com/img22/skytalk.git)
