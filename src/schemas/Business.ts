import { model, Schema } from "mongoose";

const schema = new Schema(
  {
    address: { type: String, required: true },
    city: { type: String, required: true },
    state: { type: String, required: true },
    postal_code: { type: String, required: true },
    latitude: { type: Number, required: true },
    longitude: { type: Number, required: true },
    stars: { type: Number, required: true },
    review_count: { type: Number, required: true },
  },
  { minimize: true, timestamps: true }
);

export default model("Business", schema);
